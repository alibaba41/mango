#test functions
def sub(a,b):
    return a-b
def add(a,b):
    return a+b
#Principle Component Analysis
def doPCA (data,comp):
    from sklearn.decomposition import PCA
    from sklearn.preprocessing import scale
    import matplotlib.pyplot as plt
    import numpy as np
    #scaling the data set
    data=scale(data)
    pca=PCA(n_components=comp)
    pca.fit(data)
    #plotting cumulative ratio to threshhold the no of components
    plt.plot(np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100))
    return pca

#Function to output percentage row wise in a cross table
def doCROSS (cat1,cat2,axis=int):
    import pandas as pd
    return pd.crosstab(cat1,cat2,margins=True).apply(lambda ser: ser/float(ser[-1]),axis)

#for plotting ROC curve
def doROC(train,label):
    from sklearn.metrics import roc_curve
    from sklearn.metrics import roc_auc_score
    import matplotlib.pyplot as plt
    clf=LogisticRegression(penalty='l2',C=1)
    clf.fit(train,label)
    auc=roc_auc_score(label,clf.predict_proba(train)[:, 1])
    fpr,tpr,threshold=roc_curve(label,clf.predict_proba(train)[:, 1])
    plt.plot(fpr,tpr,label="area under the curve : %0.2f" % auc)
    plt.plot([0,1],[0,1],'k--')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.legend(loc="lower right")
    plt.title("ROC curve")

#printing a tree
def printTree(clf_tree,features):
    from sklearn import tree
    tree.export_graphviz(clf_tree,out_file='tree.dot',feature_names=features)
    from sklearn.externals.six import StringIO
    import pydot
    dot_data = StringIO()
    tree.export_graphviz(clf_tree, out_file=dot_data)
    graph = pydot.graph_from_dot_data(dot_data.getvalue()) 
    a=graph.write_png("tree.png")
    from IPython.display import Image
    import os
    return Image(filename=os.getcwd()+'/tree.png')
