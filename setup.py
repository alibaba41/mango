from setuptools import setup, find_packages

setup(
    name='mango',
    version='0.3',
    packages=find_packages(),
    description='Python libraries for data science',
    long_description=open('README.md').read(),
    install_requires=['sklearn','matplotlib','numpy'],
    url='https://github.com/syeddanish41/mango',
    author='syed danish',
    author_email='syed.dani8@gmail.com'
)
